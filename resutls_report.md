Effectiveness of knowledge distillation techniques on code-analysis tasks based on large language models.
Abstract
1.	Introduction: [model distillation and code analysis]
1.1	Rationale and background:
Large language models have achieved promising results in the field of code analysis, and many pre-trained language models were fine-tuned and enhanced for the specific downstream task of code analysis (code-to-text, code representations, code summarization ..); However,  training such models require substantial amount of data storage, and processing time, which resulted in search for a method to transfer the knowledge to a smaller model, with fewer parameters using what is called knowledge distillation models. 
1.2	Objective:
In this review, our main focus is on finding answers to the following research questions:
2.	Methods:
2.1	Research Questions:
RQ1:  to what extent is model distillation applicable in the recent papers, regarding code analysis tasks.
RQ2:  in which tasks of code analysis was model distillation possible.
RQ3: Is there a shortage in the knowledge distillation for code analysis domain? If yes, what are the possible causes of it?
2.2	Search protocol:
[the need] [the action] [the materials]
In order to collect information about [specific Topic], resources were collected from numerous information sources. These sources are listed as follows:
-	Google Scholar (last used on April 13, 2023).
-	Mendeley database (last used on April 10, 2023).
-	researchGate (last used on April 13, 2023).
In addition to search engines, the following sites were useful for finding keywords that boosted the search process, and led to finding more related resources:
-	CodeXGlue [site it] (last used on April 12, 2023).
-	 https://github.com/saltudelft/ml4se/blob/master/README.md#code-completion : which includes many resources related to code-analysis tasks in deep learning (last used on April 10, 2023).
2.3	Eligibility criteria:
[introduce here]
-	Inclusion criteria:
1.	The focus of the source should be on model distillation of language models and code analysis task together.
2.	The architecture of the proposed model should be stated. 
3.	The paper should be well documented, with DOI 

-	Exclusion criteria: 
1.	the paper is a review paper.
2.	the paper results are ambiguous.
2.4	Search process:
Using the previous databases, reports related to language model distillation and code analysis were collected. During the search process, in order to add only true candidate reports, we searched for articles combining two major topics: model distillation, and code-analysis language models. It should be noted that most of the related results were published after 2018. This could be due to two main reasons:
First, the idea of model distillation was first introduced in 2015 in the paper [name of paper][cite]
Second, deep learning code models like codeBERT were introduced in 2020. [cite]
YEAR OF PUBLICATION	NO. PUBLICATIONS FOUND
2018	1
2019	2
2020	5
2021	7
2022	16
2023	5
Table 1 - number of publications found in each year
 

 
Figure 1- flow diagram of search process

3.	Results:[investigate the RQs without explicit tags]
3.1	Study selection
3.2	Study characteristics:
4.	Discussion: 
4.1	[RQ1]
4.2	[RQ2]
4.3	[RQ3]
5.	Conclusion:



